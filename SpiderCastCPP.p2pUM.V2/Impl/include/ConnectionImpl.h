/*
 * ConnectionImpl.h
 *
 *  Created on: Feb 14, 2013
 *      Author: sagibel
 */

#ifndef CONNECTIONIMPL_H_
#define CONNECTIONIMPL_H_

#include "P2PUM_Definitions.h"
#include "Connection.h"
#include "Expirable.h"
#include "Refreshable.h"
#include "Network_Messaging/Message.h"
#include "ConnectionManager.h"
#include "Listener.h"
#include "Connectable.h"
#include "ConnectionHandler.h"
#include "TimerHandler.h"

#include <boost/asio.hpp>

#define CON_ESTABLISH_TIMEOUT 10000
#define CON_HEARTBEAT_TIMEOUT 10000
#define CON_HEARTBEAT_INTERVAL 1000

using boost::asio::ip::tcp;

namespace p2pum
{

class ConnectionImpl: public Connection, public Refreshable, public Expirable, public Connectable
{

public:

        /**
         * Constructor for initiated connections.
         *
         * @param service the boost::io_service object.
         * @param params the connection parameters.
         * @return
         */
        ConnectionImpl(IOService_SPtr service, const ConnectionParams& params, Handler_SPtr _handler, TimerHandler_SPtr th);

        /**
         * Constructor for received connections.
         *
         * @param service the boost::io_service object.
         * @return
         */
        ConnectionImpl(IOService_SPtr service, Handler_SPtr _handler, TimerHandler_SPtr th);

        virtual ~ConnectionImpl();

        /*------------------------Connection methods------------------------*/

        const ConnectionID getConnectionID() const;

        const Status getStatus() const;

        const ConnectionParams& getConnectionParams() const;

        void close() throw(P2PUMException);

        void setStatus(Connection::Status s);

    	void setConnectionParams(ConnectionParams _params);


        /*----------------------Refreshable & Expirable methods---------------------*/

        long timeToRefresh() const;

        long timeToExpire() const;

        bool isExpired() const;

        bool isFresh() const;

        void refresh();

        void reset();

        void expire();

        void stale();

        /*----------------------Connectable methods------------------------------*/

        void writeToSocket(Message_SPtr message);


        void readFromSocket();

        void readContinue(const boost::system::error_code& code, size_t bytes_transferred, size_t bytes_expected);

        void connect(tcp::endpoint target);

        /********************************Auxiliary methods******************************/

        void addRemoteServerPort(int serverPort);

        void addLocalServerPort(int serverPort);

        void addConnectionInfo();

        void startConnectionEstablish();


private:
//      tcp::socket sock;
        boost::asio::deadline_timer expirationTimer;
        boost::asio::deadline_timer refreshTimer;
        Connection::Status status;
        ConnectionID id;
        Handler_SPtr handler;
        TimerHandler_SPtr timerHandler;
};

}

#endif /* CONNECTIONIMPL_H_ */
