/*
 * QueueManager.h
 *
 *  Created on: Dec 29, 2012
 *      Author: sagibel
 */

#ifndef QUEUEMANAGER_H_
#define QUEUEMANAGER_H_

#include <queue>
#include <stdbool.h>

#include <boost/shared_ptr.hpp>
#include <boost/thread/condition_variable.hpp>
#include <boost/thread/mutex.hpp>

#include "EventImpl.h"
#include "RxMessageImpl.h"
#include "Log.h"

#define EVENT_QUEUE_SIZE 100
#define MESSAGE_QUEUE_SIZE 100
#define LOG_QUEUE_SIZE 100

using std::queue;

namespace p2pum
{

/**
 * Manages the producer-consumer queues of Messages and Events.
 * Please note that access to the queue of Events should be synchronized because 2 different threads
 * (ServiceThread, Maintainer) can create and push Events to the event queue.
 */
class QueueManager{

public:

	QueueManager(const PropertyMap& properties);
	virtual ~QueueManager();

	void pushEvent(Event_SPtr& event);

	Event_SPtr popEvent();

	void pushMessage(RxMessage_SPtr& message);

	RxMessage_SPtr popMessage();

	void pushLog(Log_SPtr& log);

	Log_SPtr popLog();

	bool hasEvents();

	bool hasMessages();

	bool hasLogs();

	void waitForAny();


private:
	queue<Event_SPtr> eventQueue;
	queue<RxMessage_SPtr> messageQueue;
	queue<Log_SPtr> logQueue;

	int eventQueueSize;
	int messageQueueSize;
	int logQueueSize;

	boost::mutex eventMutex;
	boost::mutex messageMutex;
	boost::mutex logMutex;
	boost::mutex anyMutex;
	boost::condition_variable eventQueueCond;
	boost::condition_variable messageQueueCond;
	boost::condition_variable logQueueCond;
	boost::condition_variable anyQueueCond; //for whence both queues are empty
};

typedef boost::shared_ptr<QueueManager> QueueManager_SPtr;

}

#endif /* QUEUEMANAGER_H_ */
