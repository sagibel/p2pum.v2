/*
 * StringException.h
 *
 *  Created on: May 15, 2013
 *      Author: user
 */

#ifndef STRINGEXCEPTION_H_
#define STRINGEXCEPTION_H_

#include "P2PUMException.h"

namespace p2pum
{

/**
 * Represents an exception that only carries a message and a code.
 */
class StringException: public p2pum::P2PUMException
{
public:
	StringException(p2pum::ErrorCode code, std::string message) throw();
	virtual ~StringException() throw();
};

} /* namespace p2pum */
#endif /* STRINGEXCEPTION_H_ */
