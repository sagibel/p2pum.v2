/*
 * ArgumentException.h
 *
 *  Created on: May 11, 2013
 *      Author: p2pm
 */

#ifndef ARGUMENTEXCEPTION_H_
#define ARGUMENTEXCEPTION_H_

#include "P2PUMException.h"

#include <vector>

namespace p2pum
{

/**
 * An exception indicating that certain arguments are missing for a requested operation.
 */
class ArgumentException: public p2pum::P2PUMException
{
public:
	ArgumentException(std::vector<std::string> args) throw();
	virtual ~ArgumentException() throw();

private:
	std::string getMessage(std::vector<std::string> args);
};

} /* namespace p2pum */
#endif /* ARGUMENTEXCEPTION_H_ */
