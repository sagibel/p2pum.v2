/*
 * Log.h
 *
 *  Created on: Jul 3, 2013
 *      Author: user
 */

#ifndef LOG_H_
#define LOG_H_

#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>

#include "P2PUM_Definitions.h"

namespace p2pum
{

class Log
{
public:
	Log(p2pum::LogLevel _level, std::string _message);
	virtual ~Log();

	std::string getMessage();

	p2pum::LogLevel getLevel();

	uint64_t getTimestamp();

	boost::thread::id getThreadID();

private:
	std::string message;
	p2pum::LogLevel level;
	uint64_t timestamp;
	boost::thread::id threadID;
};

typedef boost::shared_ptr<Log> Log_SPtr;

} /* namespace p2pum */
#endif /* LOG_H_ */
