/*
 * Expirable.h
 *
 *  Created on: Feb 15, 2013
 *      Author: sagibel
 */

#ifndef EXPIRABLE_H_
#define EXPIRABLE_H_

#include "boost/shared_ptr.hpp"

namespace p2pum
{

/**
 * An interface for objects that have a limited operating time.
 */
class Expirable{

public:
	Expirable();
	virtual ~Expirable();

	/**
	 *
	 * @return the time in milliseconds that's left till this object becomes expired
	 */
	virtual long timeToExpire() const = 0;

	/**
	 *
	 * @return true if this object has expired and is not longer refreshable, false otherwise
	 */
	virtual bool isExpired() const = 0;

	/**
	 * Resets the expiration time to a maximum.
	 */
	virtual void reset() = 0;

	/**
	 * Stops the expirable property
	 */
	virtual void expire() = 0;

};

typedef boost::shared_ptr<Expirable> Expirable_SPtr;

}

#endif /* EXPIRABLE_H_ */
