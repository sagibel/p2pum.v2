/*
 * ConnectionManager.h
 *
 *  Created on: Dec 29, 2012
 *      Author: sagibel
 */

#ifndef CONNECTIONMANAGER_H_
#define CONNECTIONMANAGER_H_

#include <queue>
#include <map>

#include <boost/shared_ptr.hpp>
#include <boost/heap/fibonacci_heap.hpp>
#include <boost/thread/shared_mutex.hpp>

#include "P2PUM_Definitions.h"
#include "Refreshable.h"
#include "Expirable.h"
#include "AbstractStream.h"
#include "Connection.h"
#include "IdGenerator.h"

#define MAX_CONNECTIONS 100 //default max connections per instance
#define MAX_STREAMS_PER_CONNECTION 10 //default max streams per connection

using std::map;
using std::pair;

namespace p2pum
{

/**
 * Monitors the state of Connections and incoming and outgoing Streams.
 * Please not that ALL access to the containers in this class should be synchronized,
 * as all the threads (Listener, Maintainer, Communicator and application thread) access it.
 */
class ConnectionManager{

public:
	ConnectionManager(const PropertyMap& properties);
	virtual ~ConnectionManager();

	/**
	 * Adds the connection object to the list of existing connections.
	 *
	 * @param connection
	 */
	void addConnection(Connection_SPtr connection);

	/**
	 * Removes the connection that corresponds to the given id from the list of
	 * existing connections and from maintenance.
	 *
	 * @param id
	 */
//	void removeConnection(ConnectionID id);

	/**
	 * Updates the status of the connection with the given id and executes additional tasks accordingly
	 * (i.e. - when status is 'established' creates Expirable and Refreshable objects for the connection).
	 *
	 * @param connection
	 * @param status
	 */
	void updateConnectionStatus(ConnectionID id, Connection::Status status);

	/**
	 * Returns the Connection object associated with the given ConnectionID.
	 *
	 * @param id
	 * @return
	 */
	Connection_SPtr getConnection(ConnectionID id);

	/**
	 * Adds the given stream to the list of existing streams.
	 *
	 * @param stream
	 */
	void addStream(Stream_SPtr stream);

	/**
	 * Updates the status of the stream with the given id and executes additional tasks accordingly
	 * (i.e. - when status is 'open' creates Expirable and/or Refreshable objects for the stream according to its type).
	 *
	 * @param id
	 * @param status
	 */
	void updateStreamStatus(ConnectionID conID, StreamID streamID, AbstractStream::Status status);

	/**
	 * Removes the Stream that corresponds to the given id.
	 *
	 * @param id
	 */
//	void removeStream(ConnectionID conID, StreamID streamID);

	/**
	 * Returns the Stream object associated with the given StreamID.
	 *
	 * @param id
	 * @return
	 */
	Stream_SPtr getStream(ConnectionID conID, StreamID streamID);

	/**
	 * Sends a heartbeat for the stream that corresponds to the given stream id or for
	 * the given connection if the given id is -1.
	 * @param con
	 * @param stream
	 */
	void sendHeartbeat(ConnectionID conID, StreamID strID = NO_ID);

	/**
	 * Receive heartbeat for a stream and/or a connection.
	 * @param id
	 */
	void receiveHeartbeat(ConnectionID conID, StreamID strID = NO_ID);

	/**
	 * Check whether the Connection with the given ConnectionID is active.
	 * @param id
	 * @return
	 */
	bool isConActive(ConnectionID id);

	/**
	 * Check whether the Stream with the given StreamID is active.
	 * @param id
	 * @return
	 */
	bool isStreamActive(ConnectionID conID, StreamID streamID);

	std::vector< std::pair<ConnectionID, StreamID> > getActiveIncomingStreams();

private:

	map<ConnectionID, Connection_SPtr> connections; //all connections
	map<ConnectionID, map<StreamID, Stream_SPtr> > streams; //all streams

	boost::shared_mutex mutexConnections;
	boost::shared_mutex mutexStreams;

	unsigned int max_cons;
	unsigned int max_streams_per_con;
};

typedef boost::shared_ptr<ConnectionManager> ConnectionManager_SPtr;

}

#endif /* CONNECTIONMANAGER_H_ */
