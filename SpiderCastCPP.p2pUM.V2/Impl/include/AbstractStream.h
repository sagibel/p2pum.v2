/*
 * Stream.h
 *
 *  Created on: Feb 14, 2013
 *      Author: sagibel
 */

#ifndef ABSTRACTSTREAM_H_
#define ABSTRACTSTREAM_H_

#include "P2PUM_Definitions.h"

#define STREAM_HEARTBEAT_INTERVAL 1000
#define STREAM_HEARTBEAT_TIMEOUT 10000

namespace p2pum
{

class AbstractStream
{

public:

/*	enum Type
	{
		Incomig, Outgoing, Duplex
	};
*/
	enum Status
	{
		/** In establishment process				*/
		Pending,
		/** Working									*/
		Open,
		/** Closed gracefully						*/
		Closed,
		/** Closed unexpectedly						*/
		Broken,
		/** Closed as a result of mis-maintenance   */
		Expired,
		/** Rejected by receiver                     */
		Rejected,
		/** Dummy status                             */
		No_Status
	};

	AbstractStream(ConnectionID id, const PropertyMap& _props);
	virtual ~AbstractStream();

	const Status getStatus() const;

//	virtual const Type getType() const = 0;

	const StreamID getLocalStreamID() const;

	const StreamID getPeerStreamID() const;

	void setPeerStreamID(StreamID id);

	const ConnectionID getConnectionID() const;

	void setStatus(Status s);

	PropertyMap& getProps();

//	virtual void writeToStream() = 0;

//	virtual void readFromStream() = 0;

protected:
	ConnectionID conID;
	StreamID lsID;
	StreamID psID;
	Status status;
//	int heartbeat_timeout;
//	int heartbeat_interval;
	PropertyMap props;
};

typedef boost::shared_ptr<AbstractStream> Stream_SPtr;

}
#endif /* ABSTRACTSTREAM_H_ */
