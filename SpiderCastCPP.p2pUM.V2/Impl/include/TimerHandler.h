/*
 * MaintainerV2.h
 *
 *  Created on: Jun 18, 2013
 *      Author: user
 */

#ifndef MAINTAINERV2_H_
#define MAINTAINERV2_H_

#include "QueueManager.h"
#include "ConnectionManager.h"
#include "P2PUM_Definitions.h"

#include <boost/enable_shared_from_this.hpp>
#include <boost/shared_ptr.hpp>

using boost::system::error_code;

namespace p2pum {

class TimerHandler: public boost::enable_shared_from_this<TimerHandler> {
public:
	TimerHandler(QueueManager_SPtr qm, ConnectionManager_SPtr cm);
	virtual ~TimerHandler();

	void handleExpiredConnection(const error_code& code, ConnectionID id);

	void handleExpiredConnectionEstablish(const error_code& code, ConnectionID id);

	void handleExpiredStream(const error_code& code, ConnectionID cid, StreamID sid);

	void handleConnectionRefresh(const error_code& code, ConnectionID id);

	void handleStreamRefresh(const error_code& code, ConnectionID cid, StreamID sid);

private:
	QueueManager_SPtr qManager;
	ConnectionManager_SPtr cManager;
};

typedef boost::shared_ptr<TimerHandler> TimerHandler_SPtr;

} /* namespace p2pum */
#endif /* MAINTAINERV2_H_ */
