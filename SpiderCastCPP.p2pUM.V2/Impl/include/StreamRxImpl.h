/*
 * StreamRxImpl.h
 *
 *  Created on: Apr 18, 2013
 *      Author: sagibel
 */

#ifndef STREAMRXIMPL_H_
#define STREAMRXIMPL_H_

#include "StreamRx.h"
#include "ConnectionManager.h"
#include "Communicator.h"
#include "ConnectionHandler.h"


namespace p2pum
{

class StreamRxImpl: public p2pum::StreamRx
{
public:
	StreamRxImpl(ConnectionManager_SPtr _cManager, Communicator_SPtr _comm, Handler_SPtr _handler, const PropertyMap& _props);
	virtual ~StreamRxImpl();

	void close() throw (P2PUMException);

	void rejectStream(ConnectionID cid, StreamID sid) throw (P2PUMException);

private:
	ConnectionManager_SPtr cManager;
	Communicator_SPtr comm;
	Handler_SPtr handler;
	PropertyMap props;
	bool isOpen;

	void rejectStreamAux(ConnectionID cid, StreamID sid);
};

}

#endif /* STREAMRXIMPL_H_ */
