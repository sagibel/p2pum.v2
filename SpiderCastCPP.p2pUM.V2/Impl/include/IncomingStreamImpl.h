/*
 * Stream.h
 *
 *  Created on: Dec 29, 2012
 *      Author: sagibel
 */

#ifndef STREAM_H_
#define STREAM_H_

#include "P2PUM_Definitions.h"
#include "AbstractStream.h"
#include "Expirable.h"
#include "TimerHandler.h"

namespace p2pum
{

/**
 * A Wrapper class to represent expirable, refreshable streams
 */
class IncomingStreamImpl: public AbstractStream, public Expirable
{
public:

	IncomingStreamImpl(/*Type type, */ConnectionID conID, const PropertyMap& props, TimerHandler_SPtr handler, IOService_SPtr service);
	virtual ~IncomingStreamImpl();

	long timeToExpire() const;

	bool isExpired() const;

	void reset();

	void expire();

private:
//	Type type;
	boost::asio::deadline_timer timer;
	long heartbeatTimeout;
	TimerHandler_SPtr timerHandler;
};

}

#endif /* STREAM_H_ */
