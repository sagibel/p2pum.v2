/*
 * ConnectionHandler.h
 *
 *  Created on: Apr 7, 2013
 *      Author: leo9385
 */

#ifndef CONNECTIONHANDLER_H_
#define CONNECTIONHANDLER_H_

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

#include "Network_Messaging/Message.h"
#include "P2PUM_Definitions.h"

using boost::system::error_code;


namespace p2pum
{

class ConnectionHandler: public boost::enable_shared_from_this<ConnectionHandler>
{
public:
	ConnectionHandler();
	virtual ~ConnectionHandler();

	/**
	 * Handles an asynchronous read of data from the connection.
	 */
	virtual void handleRead(char* data, ConnectionID conID, const error_code& code, size_t bytes_transferred, size_t bytes_expected) = 0;

	/**
	 * Handles an asynchronous write of data to the connection.
	 */
	virtual void handleWrite(Message_SPtr message, ConnectionID conID, const error_code& code, size_t bytes_transferred, size_t bytes_expected) = 0;

	/**
	 * Handles an asynchronous connect operation for a connection.
	 */
	virtual void handleConnect(ConnectionID conID, const error_code& code) = 0;

	/**
	 * Handles an asynchronous accept of a connection through the server socket.
	 */
	virtual void handleAccept(Connection_SPtr con, const error_code& code) = 0;
};

typedef boost::shared_ptr<ConnectionHandler> Handler_SPtr;

} /* namespace p2pum */
#endif /* CONNECTIONHANDLER_H_ */
