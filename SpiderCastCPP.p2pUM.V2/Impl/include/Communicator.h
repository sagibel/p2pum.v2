/*
 * Communicator.h
 *
 *  Created on: Dec 29, 2012
 *      Author: sagibel
 */

#ifndef COMMUNICATOR_H_
#define COMMUNICATOR_H_

#include "QueueManager.h"
#include "ConnectionManager.h"
#include "Threadable.h"
#include "EventListener.h"
#include "MessageListener.h"
#include "LogListener.h"
#include "ConnectionHandler.h"

#include <boost/shared_ptr.hpp>

#include <stdbool.h>

namespace p2pum
{

/**
 * This thread waits on the QueueManager and relays Events and RxMessages to the application layer.
 */
class Communicator: public Threadable{

public:

	/**
	 * The policy for conveying messages and events to the application.
	 */
	enum Policy
	{
		/* Event and Message queues will be accessed a-la round robin.This is the default Policy */
		Balanced,
		/* Events queue has seniority - always accessed first */
		EventsFirst,
		/* Messages queue has seniority - always accessed first */
		MessagesFirst,
		/* Events have probability of policyParam, Messages of (1 - policyParam) */
		Probabilistic
	};

	/**
	 * Default constructor using the default policy - balanced.
	 * To change policy use setPolicy.
	 * @return
	 */
	Communicator(EventListener* _listener, LogListener* log, QueueManager_SPtr _qManager, ConnectionManager_SPtr _cManager, Handler_SPtr _handler);

	virtual ~Communicator();


	/**
	 * Sets the given policy as the policy of this Communicator instance.
	 */
	void setPolicy(Policy p, double param = 0.5);

	/**
	 * Registers the given MessageListener as the one to be used for relaying Messages to the application layer.
	 * This method should be called whenever a new StreamRx is created.
	 *
	 * Returns true if MessageListener was registered successfully, and false if there was another MessageListener
	 * already registered.
	 *
	 * @param listener
	 */
	bool registerMessageListener(MessageListener* listener);

	/**
	 * annuls the MessageListener so that incoming messages are ignored.
	 * Note - this means that a newly created StreamRx needs to have a reference to the Communicator objects
	 * because this method should be called when StreamRx's "close" method is called.
	 *
	 * Returns true if the current MessageListener was unregistered successfully, and false if there was no
	 * MessageListener registered in the first place.
	 */
	bool unregisterMessageListener();

	bool acceptingStreams();

	Policy getPolicy();

	void stop();

private:
	Policy policy;
	double policyParam;
	EventListener* eListener;
	MessageListener* mListener;
	LogListener* lListener;
	QueueManager_SPtr qManager;
	ConnectionManager_SPtr cManager;
	Handler_SPtr handler;

	bool serviceEvent();
	bool serviceMessage();
	bool serviceLog();
	void waitAndServe();

	void run();
};

typedef boost::shared_ptr<Communicator> Communicator_SPtr;

}

#endif /* COMMUNICATOR_H_ */
