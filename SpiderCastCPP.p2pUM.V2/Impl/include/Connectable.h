/*
 * AbstractConnection.h
 *
 *  Created on: Apr 6, 2013
 *      Author: leo9385
 */

#ifndef ABSTRACTCONNECTION_H_
#define ABSTRACTCONNECTION_H_

#include "P2PUM_Definitions.h"
#include "ConnectionHandler.h"
#include "Network_Messaging/Message.h"

#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

using boost::asio::ip::tcp;

namespace p2pum
{

class Connectable: public boost::enable_shared_from_this<Connectable>
{
public:
	Connectable(IOService_SPtr _service);
	virtual ~Connectable();

	/**
	 * Returns the local socket used for this connection
	 * @return
	 */
	tcp::socket& getSocket();

	/**
	 * writes the given message to the socket of this connection in an asynchronous manner.
	 */
	virtual void writeToSocket(Message_SPtr message) = 0;

	/**
	 * Reads from the socket of this connection in an asynchronous manner.
	 */
	virtual void readFromSocket() = 0;

	/**
	 * Establishes a connection with the given endpoint.
	 */
	virtual void connect(tcp::endpoint target) = 0;

	/**
	 * Disconnects the socket.
	 * If socket wasn't connected or a problem occurred while disconnecting, a boost::system::system_error exception is thrown.
	 */
	void disconnect();

	/***********************Comfort & Testing Methods*******************************/

	std::string& getOutboundHeader(){ return outboundHeader;}

	std::string& getOutboundData(){ return outboundData;}

	char* getInboundHeader(){ return inboundHeader;}

	std::vector<char>& getInboundData(){ return inboundData;}


protected:
	tcp::socket sock;

	/// Holds an outbound header.
	std::string outboundHeader;

	/// Holds the outbound data.
	std::string outboundData;

	/// Holds an inbound header, sized with size_t to hold the saize of the incoming message.
	char inboundHeader[sizeof(uint64_t)];

	/// Holds the inbound data.
	std::vector<char> inboundData;

};

typedef boost::shared_ptr<Connectable> Connectable_SPtr;


} /* namespace p2pum */
#endif /* ABSTRACTCONNECTION_H_ */
