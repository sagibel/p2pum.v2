/*
 * Message.h
 *
 *  Created on: Feb 25, 2013
 *      Author: sagibel
 */

#ifndef MESSAGE_H_
#define MESSAGE_H_

#include <boost/shared_ptr.hpp>
#include <boost/serialization/export.hpp>
//#include <boost/serialization/tracking.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>


#include "P2PUM_Definitions.h"

namespace p2pum
{

class Message
{
public:

	enum MessageType{
		ConnectionSuggest,
		ConnectionResponse,
		ConnectionClose,
		ConnectionHearbeat,
		StreamSuggest,
		StreamResponse,
		StreamClose,
		StreamHeartbeat,
		Content
	};

	Message();
	virtual ~Message();

	virtual MessageType getType() = 0; //to make the class abstract

	const MessageID getMessageID() const;

	template<typename Archive>
	void serialize(Archive& ar, const unsigned int version){
		ar & id;
//		std::cout << "Message serialized" << std::endl;
	}

	MessageID id;

};

typedef boost::shared_ptr<Message> Message_SPtr;

}

BOOST_SERIALIZATION_ASSUME_ABSTRACT(p2pum::Message)
BOOST_CLASS_TRACKING(p2pum::Message, track_always)
BOOST_SERIALIZATION_SHARED_PTR(p2pum::Message)

#endif /* MESSAGE_H_ */
