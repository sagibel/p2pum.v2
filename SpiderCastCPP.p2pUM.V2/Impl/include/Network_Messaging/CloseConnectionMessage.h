/*
 * CloseConnectionMessage.h
 *
 *  Created on: Feb 27, 2013
 *      Author: sagibel
 */

#ifndef CLOSECONNECTIONMESSAGE_H_
#define CLOSECONNECTIONMESSAGE_H_

#include "ConnectionMessage.h"

#include <boost/serialization/export.hpp>

namespace p2pum
{

class CloseConnectionMessage: public p2pum::ConnectionMessage
{
public:
	CloseConnectionMessage();
	/**
	 *
	 * @param id the id of the connection at the PEER.
	 * @return
	 */
	CloseConnectionMessage(ConnectionID id);
	virtual ~CloseConnectionMessage();

	virtual Message::MessageType getType();

	template<typename Archive>
	void serialize(Archive& ar, const unsigned int version){
		ar & boost::serialization::base_object<ConnectionMessage>(*this);
//		std::cout << "CloseConnectionMessage serialized" << std::endl;
	}
};

}

BOOST_CLASS_EXPORT_KEY(p2pum::CloseConnectionMessage)

#endif /* CLOSECONNECTIONMESSAGE_H_ */
