/*
 * Expirable.h
 *
 *  Created on: Dec 29, 2012
 *      Author: sagibel
 */

#ifndef REFRESHABLE_H_
#define REFRESHABLE_H_

#include "boost/shared_ptr.hpp"

namespace p2pum
{

/**
 * An interface for objects that need to be refreshed regularly to stay operative.
 */
class Refreshable{

public:
	Refreshable();
	virtual ~Refreshable();

	/**
	 *
	 * @return the time in milliseconds that's left till this object needs to be refreshed
	 */
	virtual long timeToRefresh() const = 0;

	/**
	 *
	 * @return true if this object is fresh, false otherwise
	 */
	virtual bool isFresh() const = 0;

	/**
	 * resets the expiration time of this object to its maximum.
	 */
	virtual void refresh() = 0;

	/**
	 * Stops the refreshable property
	 */
	virtual void stale() = 0;

};

typedef boost::shared_ptr<Refreshable> Refreshable_SPtr;

}


#endif /* EXPIRABLE_H_ */
