/*
 * OutgoingStreamImpl.h
 *
 *  Created on: Feb 22, 2013
 *      Author: sagibel
 */

#ifndef OUTGOINGSTREAMIMPL_H_
#define OUTGOINGSTREAMIMPL_H_

#include "P2PUM_Definitions.h"
#include "AbstractStream.h"
#include "Refreshable.h"
#include "TimerHandler.h"

namespace p2pum
{

/**
 * A Wrapper class to represent expirable, refreshable streams
 */
class OutgoingStreamImpl: public AbstractStream, public Refreshable
{
public:

	OutgoingStreamImpl(/*Type type, */ConnectionID conID, const PropertyMap& props, TimerHandler_SPtr handler, IOService_SPtr service);
	virtual ~OutgoingStreamImpl();

	long timeToRefresh() const;


	bool isFresh() const;

	void refresh();

	void stale();

private:
//	Type type;
	long heartbeatInterval;
	boost::asio::deadline_timer timer;
	TimerHandler_SPtr timerHandler;
};

}

#endif /* OUTGOINGSTREAMIMPL_H_ */
