/*
 * Listener.h
 *
 *  Created on: Dec 29, 2012
 *      Author: sagibel
 */

#ifndef LISTENER_H_
#define LISTENER_H_

#include "ConnectionHandler.h"
#include "Threadable.h"
#include "P2PUM_Definitions.h"
#include "TimerHandler.h"

#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>

using boost::asio::ip::tcp;

namespace p2pum
{

class Listener{

public:
	Listener(IOService_SPtr _service, int _port, TimerHandler_SPtr _handler);
	virtual ~Listener();

	void close();

	void listen(Handler_SPtr handler);

	TimerHandler_SPtr getTimerHandler();

	IOService_SPtr getIOService();

private:
	tcp::acceptor serverSocket;
	IOService_SPtr service;
	TimerHandler_SPtr handler;
};

typedef boost::shared_ptr<Listener> Listener_SPtr;

}





#endif /* LISTENER_H_ */
