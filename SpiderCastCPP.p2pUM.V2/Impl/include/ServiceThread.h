/*
 * ServiceThread.h
 *
 *  Created on: May 21, 2013
 *      Author: user
 */

#ifndef SERVICETHREAD_H_
#define SERVICETHREAD_H_

#include "Threadable.h"
#include "P2PUM_Definitions.h"

#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>

namespace p2pum
{

/**
 * This class is a dedicated thread for performing the asynchronous operations of the P2PUM instance and executing the handlers
 * of those operations.
 */
class ServiceThread: public p2pum::Threadable
{
public:
	ServiceThread(IOService_SPtr _service);
	virtual ~ServiceThread();

	void stop();

private:
	IOService_SPtr servcie;
	std::auto_ptr<boost::asio::io_service::work> worker;

	void run();

};

typedef boost::shared_ptr<ServiceThread> ServiceThread_SPtr;

} /* namespace p2pum */
#endif /* SERVICETHREAD_H_ */
