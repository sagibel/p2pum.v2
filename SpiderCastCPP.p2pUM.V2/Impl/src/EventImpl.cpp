/*
 * EventImpl.cpp
 *
 *  Created on: Feb 6, 2013
 *      Author: sagibel
 */

#include "EventImpl.h"
#include "IdGenerator.h"

namespace p2pum{

	EventImpl::EventImpl(Type type, Connection_SPtr _con, ConnectionID _id): eventType(type){
		this->code = p2pum::No_Code;
		this->con = _con;
		this->conID = _id;
		this->message = "";
		this->streamID = NO_ID;
		this->tag = "";
	}

	EventImpl::EventImpl(Type type, Connection_SPtr _con, ConnectionID _id, Stream_SPtr stream): eventType(type){
		this->code = p2pum::No_Code;
		this->con = _con;
		this->conID = _id;
		this->message = "";
		this->streamID = stream->getLocalStreamID();
		PropertyMap::const_iterator it = stream->getProps().find("tag");
		if(it == stream->getProps().end()){
			this->tag = "";
		}
		else{
			this->tag = it->second;
		}
	}

	EventImpl::EventImpl(Type type, Connection_SPtr _con, ConnectionID _id, ErrorCode _code, std::string _message): eventType(type){
		this->code = _code;
		this->con = _con;
		this->conID = _id;
		this->message = _message;
		this->streamID = NO_ID;
		this->tag = "";
	}

	EventImpl::EventImpl(Type type, Connection_SPtr _con, ConnectionID _id, Stream_SPtr stream, ErrorCode _code, std::string _message): eventType(type){
		this->code = _code;
		this->con = _con;
		this->conID = _id;
		this->message = _message;
		this->streamID = stream->getLocalStreamID();
		PropertyMap::const_iterator it = stream->getProps().find("tag");
		if(it == stream->getProps().end()){
			this->tag = "";
		}
		else{
			this->tag = it->second;
		}
	}

	EventImpl::~EventImpl(){}

	Event::Type EventImpl::getType() const{
		return this->eventType;
	}

	StreamID EventImpl::getStreamID() const{
		return this->streamID;
	}

	ConnectionID EventImpl::getConnectionID() const{
		return this->conID;
	}

	Connection_SPtr EventImpl::getConnection() const{
		return this->con;
	}

	std::string EventImpl::getStreamTag() const{
		return this->tag;
	}

	ErrorCode EventImpl::getErrorCode() const{
		return this->code;
	}

	std::string EventImpl::getErrorMessge() const{
		return this->message;
	}

//	void EventImpl::setConnection(Connection_SPtr _con){
//		this->con = _con;
//	}
//
//	void EventImpl::setConnectionID(ConnectionID _id){
//		this->conID = _id;
//	}
//
//	void EventImpl::setStreamID(StreamID _id){
//		this->streamID = _id;
//	}
//
//	void EventImpl::setErrorCode(ErrorCode _code){
//		this->code = _code;
//	}
//
//	void EventImpl::setErrorMessage(std::string _message){
//		this->message = _message;
//	}
//
//	void EventImpl::setStreamTag(std::string _tag){
//		this->tag = _tag;
//	}

}
