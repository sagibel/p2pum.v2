/*
 * CloseConnectionMessage.cpp
 *
 *  Created on: Feb 27, 2013
 *      Author: sagibel
 */

#include "Network_Messaging/CloseConnectionMessage.h"

BOOST_CLASS_EXPORT_IMPLEMENT(p2pum::CloseConnectionMessage)

namespace p2pum
{

CloseConnectionMessage::CloseConnectionMessage()
{}

CloseConnectionMessage::CloseConnectionMessage(ConnectionID id): ConnectionMessage(id)
{}

CloseConnectionMessage::~CloseConnectionMessage()
{}

Message::MessageType CloseConnectionMessage::getType(){
	return Message::ConnectionClose;
}

/*
template<typename Archive>
void CloseConnectionMessage::serialize(Archive& ar, const unsigned int version){
	ar & boost::serialization::base_object<ConnectionMessage>(*this);
}*/

}
