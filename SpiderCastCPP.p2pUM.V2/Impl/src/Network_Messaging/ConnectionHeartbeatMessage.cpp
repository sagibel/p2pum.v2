/*
 * ConnectionHeartbeatMessage.cpp
 *
 *  Created on: Apr 5, 2013
 *      Author: leo9385
 */

#include "Network_Messaging/ConnectionHeartbeatMessage.h"

BOOST_CLASS_EXPORT_IMPLEMENT(p2pum::ConnectionHeartbeatMessage)


namespace p2pum
{

ConnectionHeartbeatMessage::ConnectionHeartbeatMessage()
{}

ConnectionHeartbeatMessage::ConnectionHeartbeatMessage(ConnectionID id): ConnectionMessage(id)
{}

ConnectionHeartbeatMessage::~ConnectionHeartbeatMessage()
{}

Message::MessageType ConnectionHeartbeatMessage::getType(){
	return Message::ConnectionHearbeat;
}

} /* namespace p2pum */
