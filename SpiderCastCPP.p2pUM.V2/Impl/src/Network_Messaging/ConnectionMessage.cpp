/*
 * EstablishConnectionMessage.cpp
 *
 *  Created on: Feb 25, 2013
 *      Author: sagibel
 */

#include "Network_Messaging/ConnectionMessage.h"

namespace p2pum
{

ConnectionMessage::ConnectionMessage(){

}

ConnectionMessage::ConnectionMessage(ConnectionID id): conID(id){

}

ConnectionMessage::~ConnectionMessage()
{}

}
