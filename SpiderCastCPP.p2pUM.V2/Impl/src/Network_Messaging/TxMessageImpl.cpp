/*
 * TxMessageImpl.cpp
 *
 *  Created on: Feb 25, 2013
 *      Author: sagibel
 */

#include "Network_Messaging/TxMessageImpl.h"

BOOST_CLASS_EXPORT_IMPLEMENT(p2pum::TxMessageImpl)


namespace p2pum
{

TxMessageImpl::TxMessageImpl()
{

}

TxMessageImpl::TxMessageImpl(StreamID id, char* content, MessageSize size): StreamMessage(id)
{
	buffer = content;
	length = size;
}

TxMessageImpl::~TxMessageImpl()
{}

Message::MessageType TxMessageImpl::getType(){
	return Message::Content;
}

}
