/*
 * ConnectionManager.cpp
 *
 *  Created on: Feb 14, 2013
 *      Author: sagibel
 */

#include "ConnectionManager.h"
#include "ConnectionImpl.h"
#include "IncomingStreamImpl.h"
#include "OutgoingStreamImpl.h"
#include "Exceptions/LimitException.h"

#include <boost/lexical_cast.hpp>

namespace p2pum{

	ConnectionManager::ConnectionManager(const PropertyMap& properties){
		if(properties.count("max_cons")){
			this->max_cons = boost::lexical_cast<unsigned int>(properties.find("max_cons")->second);
		}
		else{
			this->max_cons = MAX_CONNECTIONS;
		}
		if(properties.count("max_streams")){
			this->max_streams_per_con = boost::lexical_cast<unsigned int>(properties.find("max_streams")->second);
		}
		else{
			this->max_streams_per_con = MAX_STREAMS_PER_CONNECTION;
		}
	}
	ConnectionManager::~ConnectionManager(){}

	void ConnectionManager::addConnection(Connection_SPtr connection){
		boost::unique_lock<boost::shared_mutex> lock(this->mutexConnections); //write lock
		if(connections.size() >= this->max_cons){
			lock.unlock();
			throw LimitException();
		}
		connections[connection->getConnectionID()] = connection;
		lock.unlock();
	}

/*
	void ConnectionManager::removeConnection(ConnectionID id){
		boost::unique_lock<boost::shared_mutex> lock1(this->mutexConnections);
		boost::unique_lock<boost::shared_mutex> lock2(this->mutexIncoming);
		boost::unique_lock<boost::shared_mutex> lock3(this->mutexOutgoing);
		Connection_SPtr con = connections[id];
		std::pair<ConnectionID, StreamID> p(id, NO_ID);
		incoming.erase(expirableHandles[p]);
		expirableHandles.erase(p);
		outgoing.erase(refreshableHandles[p]);
		refreshableHandles.erase(p);
		connections.erase(id);
		lock3.unlock();
		lock2.unlock();
		lock1.unlock();
	}*/

	//for outgoing pending to established: add refreshable.
	//for incoming pending to established: add refreshable & expirable
	//for outgoing pending: add expirable
	//for others remove expirable & refreshable
	void ConnectionManager::updateConnectionStatus(ConnectionID id, Connection::Status status){
		boost::unique_lock<boost::shared_mutex> lock1(this->mutexConnections); //write lock because later we change the connection status
		Connection_SPtr con = connections[id];
		if(con->getStatus() == status){
			//status not changed
			lock1.unlock();
			return;
		}
		std::pair<ConnectionID, StreamID> p(id, NO_ID);
		if(status == Connection::Established){
			Refreshable_SPtr refreshable = boost::dynamic_pointer_cast<Refreshable, Connection>(con);
			refreshable->refresh();
			Expirable_SPtr expirable = boost::dynamic_pointer_cast<Expirable, Connection>(con);
			expirable->reset(); //will cancel the establish timeout tier automatically
		}
		else if(status == Connection::OutgoingPending){
			ConnectionImpl* con_ = dynamic_cast<ConnectionImpl*>(con.get());
			con_->startConnectionEstablish(); //set establish timeout timer
		}
		else if(con->getStatus() == Connection::Established){ //stop timers
			Refreshable_SPtr refreshable = boost::dynamic_pointer_cast<Refreshable, Connection>(con);
			refreshable->stale();
			Expirable_SPtr expirable = boost::dynamic_pointer_cast<Expirable, Connection>(con);
			expirable->expire();
		}
		con->setStatus(status);//set status at the end because current status is needed before
		//REMARK if status changed from established to something else - close streams associated to the connection? currently not because when sending messages both stream and connection are checked
		lock1.unlock();
	}

	Connection_SPtr ConnectionManager::getConnection(ConnectionID id){
		boost::shared_lock<boost::shared_mutex> lock(this->mutexConnections); //read lock
		return connections[id];//TODO add check for existance
	}

	void ConnectionManager::addStream(Stream_SPtr stream){
		boost::unique_lock<boost::shared_mutex> lock1(this->mutexStreams); //write lock
		if(streams[stream->getConnectionID()].size() >= this->max_streams_per_con){
			lock1.unlock();
			throw LimitException(stream->getConnectionID());
		}
		streams[stream->getConnectionID()][stream->getLocalStreamID()] = stream;
		lock1.unlock();
	}

	void ConnectionManager::updateStreamStatus(ConnectionID conID, StreamID streamID, AbstractStream::Status status){
		boost::unique_lock<boost::shared_mutex> lock1(this->mutexStreams); //write lock because later we change the status of the stream
		Stream_SPtr stream = streams[conID][streamID];
		if(stream->getStatus() == status){
			//status not changed
			lock1.unlock();
			return;
		}
		std::pair<ConnectionID, StreamID> p(conID, streamID);
		if(status == AbstractStream::Open){
			if(typeid(*stream) == typeid(IncomingStreamImpl)){
				Expirable_SPtr expirable = boost::dynamic_pointer_cast<Expirable, AbstractStream>(stream);
				expirable->reset();
			}
			else{
				Refreshable_SPtr refreshable = boost::dynamic_pointer_cast<Refreshable, AbstractStream>(stream);
				refreshable->refresh();
			}
		}
		else if(stream->getStatus() == AbstractStream::Open){ //stop timer
			if(typeid(*stream) == typeid(IncomingStreamImpl)){
				Expirable_SPtr expirable = boost::dynamic_pointer_cast<Expirable, AbstractStream>(stream);
				expirable->expire();
			}
			else{
				Refreshable_SPtr refreshable = boost::dynamic_pointer_cast<Refreshable, AbstractStream>(stream);
				refreshable->stale();
			}
		}
		stream->setStatus(status); //set status at the end because current status is needed before
		lock1.unlock();
	}
/*
	void ConnectionManager::removeStream(ConnectionID conID, StreamID streamID){
		Stream_SPtr stream = streams[conID][streamID];
		std::pair<ConnectionID, StreamID> p(conID, streamID);
		if(stream->getStatus() == AbstractStream::Open){
			this->updateStreamStatus(conID, streamID, AbstractStream::Broken);
		}
		streams[conID].erase(streamID);
	}
*/
	Stream_SPtr ConnectionManager::getStream(ConnectionID conID, StreamID streamID){
		boost::shared_lock<boost::shared_mutex> lock(this->mutexStreams);
		return streams[conID][streamID]; //TODO check existence
	}

	void ConnectionManager::sendHeartbeat(ConnectionID conID, StreamID strID){
		Refreshable_SPtr ref;
		if(strID == NO_ID){
			Connection_SPtr con = connections[conID];
			ref = boost::dynamic_pointer_cast<Refreshable, Connection>(con);
		}
		else{
			Stream_SPtr stream = streams[conID][strID];
			ref = boost::dynamic_pointer_cast<Refreshable, AbstractStream>(stream);
		}
		ref->refresh();
	}

	void ConnectionManager::receiveHeartbeat(ConnectionID conID, StreamID strID){
		Expirable_SPtr exp;
		if(strID == NO_ID){
			Connection_SPtr con = connections[conID];
			exp = boost::dynamic_pointer_cast<Expirable, Connection>(con);
		}
		else{
			Stream_SPtr stream = streams[conID][strID];
			exp = boost::dynamic_pointer_cast<Expirable, AbstractStream>(stream);
		}
		exp->reset();
	}

	bool ConnectionManager::isConActive(ConnectionID id){
		boost::shared_lock<boost::shared_mutex> lock(this->mutexConnections); //read lock
		return (connections[id]->getStatus() == Connection::Established);
	}

	bool ConnectionManager::isStreamActive(ConnectionID conID, StreamID streamID){
		boost::shared_lock<boost::shared_mutex> lock(this->mutexStreams); //read lock
		return (streams[conID][streamID]->getStatus() == AbstractStream::Open);
	}

	std::vector< std::pair<ConnectionID, StreamID> > ConnectionManager::getActiveIncomingStreams(){
		std::vector< std::pair<ConnectionID, StreamID> > incoming;
		boost::shared_lock<boost::shared_mutex> lock(this->mutexStreams);//read lock to locate all incoming streams
		for(map<ConnectionID, map<StreamID, Stream_SPtr> >::iterator conToStreams = this->streams.begin(); conToStreams != this->streams.end(); ++conToStreams){
			for(map<StreamID, Stream_SPtr>::iterator it = conToStreams->second.begin(); it != conToStreams->second.end(); ++it){
				Stream_SPtr ptr = it->second;
				if((typeid(*ptr) == typeid(IncomingStreamImpl)) && (ptr->getStatus() == AbstractStream::Open)){
					incoming.push_back(std::pair<ConnectionID, StreamID>(ptr->getConnectionID(), ptr->getLocalStreamID()));
				}
			}
		}
		lock.unlock();
		return incoming;
	}

}
