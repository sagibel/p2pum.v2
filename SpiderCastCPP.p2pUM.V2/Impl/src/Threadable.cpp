/*
 * Threadable.cpp
 *
 *  Created on: Mar 5, 2013
 *      Author: sagibel
 */

#include "Threadable.h"

namespace p2pum
{

	Threadable::Threadable(){}
	Threadable::~Threadable(){}

	void Threadable::start(){
		this->thread = boost::thread(&Threadable::run, this);
	}

	void Threadable::join(){
		this->thread.join();
	}
}
