/*
 * SystemException.cpp
 *
 *  Created on: May 15, 2013
 *      Author: user
 */

#include "Exceptions/SystemException.h"

namespace p2pum
{

SystemException::SystemException(p2pum::ErrorCode code, std::string message) throw(): P2PUMException(message, code)
{}

SystemException::~SystemException() throw()
{}

} /* namespace p2pum */
