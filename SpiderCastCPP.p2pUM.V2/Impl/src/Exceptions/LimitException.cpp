/*
 * LimitException.cpp
 *
 *  Created on: May 11, 2013
 *      Author: p2pm
 */

#include "Exceptions/LimitException.h"

namespace p2pum
{

LimitException::LimitException(ConnectionID id) throw(): P2PUMException(getMessage(id), getCode(id))
{
	this->conID = id;
}

LimitException::~LimitException() throw()
{}

p2pum::ErrorCode LimitException::getCode(ConnectionID id){
	if(id == NO_ID){
		return p2pum::Connections_Limit_Reached;
	}
	else{
		return p2pum::Streams_Limit_Reached;
	}
}

std::string LimitException::getMessage(ConnectionID id){
	if(id == NO_ID){
		return std::string("The maximum amount of connections have been reached for this P2PUM instance");
	}
	else{
		std::ostringstream stream;
		stream << "The maximum amount of streams have been reached for connection: " << id;
		return stream.str();
	}
}

} /* namespace p2pum */
