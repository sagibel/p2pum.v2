/*
 * Communicator.cpp
 *
 *  Created on: Feb 25, 2013
 *      Author: sagibel
 */

#include <stdlib.h>
#include <time.h>

#include "Communicator.h"
#include "Connectable.h"
#include "Network_Messaging/Message.h"
#include "Network_Messaging/ConnectionResponseMessage.h"
#include "Network_Messaging/StreamResponseMessage.h"
#include "Log.h"

namespace p2pum
{

	Communicator::Communicator(EventListener* _listener, LogListener* log, QueueManager_SPtr _qManager, ConnectionManager_SPtr _cManager, Handler_SPtr _handler): policy(Communicator::Balanced), policyParam(0){
		this->cManager = _cManager;
		this->eListener = _listener;
		this->qManager = _qManager;
		this->mListener = NULL;
		this->handler = _handler;
		this->lListener = log;
	}

	Communicator::~Communicator(){}

	void Communicator::setPolicy(Policy p, double param){
		if(param < 1 && param > 0){
			policyParam = param;
		}
		else{
			//TODO exception!
		}
	}

	bool Communicator::registerMessageListener(MessageListener* listener){
		if(this->mListener != NULL){
			return false;
		}
		this->mListener = listener;
		return true;
	}

	bool Communicator::unregisterMessageListener(){
		if(this->mListener == NULL){
			return false;
		}
		this->mListener = NULL;
		return true;
	}

	bool Communicator::acceptingStreams(){
		return (this->mListener != NULL);
	}

	Communicator::Policy Communicator::getPolicy(){
		return policy;
	}

	void Communicator::run(){
		if(policy == Communicator::Balanced){
			int last = 0;
			while(!thread.interruption_requested()){
				if(last == 0){
					while(!serviceEvent() && !serviceMessage() && !serviceLog()){
						qManager->waitForAny(); //wait till event or message arrives;
					}
				}
				else if(last == 1){
					while(!serviceMessage() && !serviceLog() && !serviceEvent()){
						qManager->waitForAny(); //wait till event or message arrives;
					}
				}
				else{
					while(!serviceLog() && !serviceEvent() && !serviceMessage()){
						qManager->waitForAny(); //wait till event or message arrives;
					}
				}
				last = ((last + 1) % 3);
			}
		}
		else if(policy == Communicator::EventsFirst){
			while(!thread.interruption_requested()){
				if(!serviceEvent() && !serviceMessage() && !serviceLog()){
					qManager->waitForAny(); //wait till event or message arrives;
				}
			}
		}
		else if(policy == Communicator::MessagesFirst){
			while(!thread.interruption_requested()){
				if(!serviceMessage() && !serviceEvent() && !serviceLog()){
					qManager->waitForAny(); //wait till event or message arrives;
				}
			}
		}
		else if(policy == Communicator::Probabilistic){
			srand(time(NULL));
			while(!thread.interruption_requested()){
				int draw = rand() % 100;
				if((double)draw < (policyParam*100)){
					while(!serviceEvent() && !serviceMessage() && !serviceLog()){
						qManager->waitForAny(); //wait till event or message arrives;
					}
				}
				else{
					while(!serviceMessage() && !serviceEvent() && !serviceLog()){
						qManager->waitForAny(); //wait till event or message arrives;
					}
				}
			}
		}
	}

	void Communicator::stop(){
		this->thread.interrupt();
	}


	bool Communicator::serviceEvent(){
		EventReturnCode erc;
		Event_SPtr event;
		if(qManager->hasEvents()){
			event = qManager->popEvent();
			erc = eListener->onEvent(boost::ref(*event));
			if(event->getType() == Event::Connection_New){
				Message_SPtr message;
				Connection_SPtr con = cManager->getConnection(event->getConnectionID());
				Connectable_SPtr connectable = boost::dynamic_pointer_cast<Connectable, Connection>(con);
				if((erc == p2pum::Event_RC_Accept) && (con->getStatus() == Connection::IncomingPending)){
					message = Message_SPtr(new ConnectionResponseMessage(event->getConnectionID(), true));
					connectable->writeToSocket(message);

					cManager->updateConnectionStatus(event->getConnectionID(), Connection::Established);
				}
				else{ //if returns NoOp, treat as reject
					message = Message_SPtr(new ConnectionResponseMessage(event->getConnectionID(), false));
					connectable->writeToSocket(message);
					cManager->updateConnectionStatus(event->getConnectionID(), Connection::Failed);
				}
			}
			if(event->getType() == Event::Rx_New_Source){
				Message_SPtr message;
				Connectable_SPtr con = boost::dynamic_pointer_cast<Connectable, Connection>(cManager->getConnection(event->getConnectionID()));
				Stream_SPtr stream = this->cManager->getStream(event->getConnectionID(), event->getStreamID());
				if(erc == p2pum::Event_RC_Accept && (stream->getStatus() == AbstractStream::Pending)){
					message = Message_SPtr(new StreamResponseMessage(stream->getPeerStreamID(), true, stream->getLocalStreamID()));
					con->writeToSocket(message);
					cManager->updateStreamStatus(event->getConnectionID(), event->getStreamID(), AbstractStream::Open);
				}
				else{ //if returns NoOp, treat as reject
					message = Message_SPtr(new StreamResponseMessage(stream->getPeerStreamID(), false, stream->getLocalStreamID()));
					con->writeToSocket(message);
					cManager->updateStreamStatus(event->getConnectionID(), event->getStreamID(), AbstractStream::Rejected);

				}
			}
			return true;
		}
		return false;
	}

	bool Communicator::serviceMessage(){
		RxMessage_SPtr message;
		if((this->mListener != NULL) && qManager->hasMessages()){
			message = qManager->popMessage();
			mListener->onMessage(*message);
			return true;
		}
		return false;
	}

	bool Communicator::serviceLog(){
		Log_SPtr message;
		if(qManager->hasLogs()){
			message = qManager->popLog();
//			lListener->onLogMessage(message->getLevel(), message->getTimestamp(), message->getThreadID(), message->getMessage()); //TODO cast boost thread id to uint64_t??
			return true;
		}
		return false;
	}
}
