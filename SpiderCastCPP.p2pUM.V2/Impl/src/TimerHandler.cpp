/*
 * MaintainerV2.cpp
 *
 *  Created on: Jun 18, 2013
 *      Author: user
 */

#include "TimerHandler.h"
#include "Connectable.h"
#include "Refreshable.h"
#include "Expirable.h"
#include "AbstractStream.h"
#include "EventImpl.h"
#include "Network_Messaging/ConnectionHeartbeatMessage.h"
#include "Network_Messaging/StreamHeartbeatMessage.h"


namespace p2pum {

TimerHandler::TimerHandler(QueueManager_SPtr qm, ConnectionManager_SPtr cm) {
	this->cManager = cm;
	this->qManager = qm;
}

TimerHandler::~TimerHandler() {}

void TimerHandler::handleExpiredConnection(const error_code& code, ConnectionID id){
	if(code == boost::asio::error::operation_aborted){
		return;
	}
	Connection_SPtr con = cManager->getConnection(id);
	cManager->updateConnectionStatus(id, Connection::Broken);
	Event_SPtr event(new EventImpl(Event::Connection_Heartbeat_Timeout, con, id));
	this->qManager->pushEvent(event);
}

void TimerHandler::handleExpiredConnectionEstablish(const error_code& code, ConnectionID id){
	if(code == boost::asio::error::operation_aborted){
		return;
	}
	Connection_SPtr con = cManager->getConnection(id);
	cManager->updateConnectionStatus(id, Connection::Failed);
	Event_SPtr event(new EventImpl(Event::Connection_Establish_Timeout, con, id));
	this->qManager->pushEvent(event);
}

void TimerHandler::handleExpiredStream(const error_code& code, ConnectionID cid, StreamID sid){
	if(code == boost::asio::error::operation_aborted){
		return;
	}
	cManager->updateStreamStatus(cid, sid, AbstractStream::Expired);
	Event_SPtr event(new EventImpl(Event::Rx_Heartbeat_Timeout, cManager->getConnection(cid), cid, cManager->getStream(cid, sid)));
	this->qManager->pushEvent(event);
}

void TimerHandler::handleConnectionRefresh(const error_code& code, ConnectionID id){
	if(code == boost::asio::error::operation_aborted){
		return;
	}
	Connection_SPtr con = cManager->getConnection(id);
	Connectable_SPtr connectable = boost::dynamic_pointer_cast<Connectable, Connection>(con);
	Message_SPtr message(new ConnectionHeartbeatMessage(/*con->getConnectionID()*/));
	connectable->writeToSocket(message);
//	Refreshable_SPtr ref = boost::dynamic_pointer_cast<Refreshable, Connection>(con);
//	ref->refresh(); //sets the timer going - REMARK done in connection handler
}

void TimerHandler::handleStreamRefresh(const error_code& code, ConnectionID cid, StreamID sid){
	if(code == boost::asio::error::operation_aborted){
		return;
	}
	Stream_SPtr stream = cManager->getStream(cid, sid);
	Connectable_SPtr con = boost::dynamic_pointer_cast<Connectable, Connection>(cManager->getConnection(stream->getConnectionID()));
	Message_SPtr message(new StreamHeartbeatMessage(stream->getPeerStreamID()));
	con->writeToSocket(message);
//	Refreshable_SPtr ref = boost::dynamic_pointer_cast<Refreshable, Connectable>(con);
//	ref->refresh(); //sets the timer going - REMARK done in connection handler
}

} /* namespace p2pum */
