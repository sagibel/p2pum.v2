/*
 * Log.cpp
 *
 *  Created on: Jul 3, 2013
 *      Author: user
 */

#include "Log.h"

#include <time.h>

namespace p2pum
{

Log::Log(p2pum::LogLevel _level, std::string _message)
{
	this->level = _level;
	this->message = _message;
	this->timestamp = time(NULL);
	this->threadID = boost::this_thread::get_id();
}

Log::~Log()
{}

std::string Log::getMessage(){
	return this->message;
}

p2pum::LogLevel Log::getLevel(){
	return this->level;
}

uint64_t Log::getTimestamp(){
	return this->timestamp;
}

boost::thread::id Log::getThreadID(){
	return this->threadID;
}

} /* namespace p2pum */
