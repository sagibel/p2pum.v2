/*
 * IncomingStreamImpl.cpp
 *
 *  Created on: Mar 5, 2013
 *      Author: sagibel
 */

#include "IncomingStreamImpl.h"

#include <boost/bind.hpp>

namespace p2pum
{
	IncomingStreamImpl::IncomingStreamImpl(/*Type type, */ConnectionID conID, const PropertyMap& props, TimerHandler_SPtr handler, IOService_SPtr service): AbstractStream(conID, props), timer(*service){
		PropertyMap::const_iterator it = props.find("heartbeat_timeout");
		if(it == props.end()){
			this->heartbeatTimeout = STREAM_HEARTBEAT_TIMEOUT;
		}
		else{
			this->heartbeatTimeout = atoi(it->second.c_str());
		}
		this->timerHandler = handler;
		reset();
	}
	IncomingStreamImpl::~IncomingStreamImpl(){}

	long IncomingStreamImpl::timeToExpire() const{
		return this->timer.expires_from_now().total_milliseconds();
	}

	bool IncomingStreamImpl::isExpired() const{
		return (timeToExpire() <= 0);
	}

	void IncomingStreamImpl::reset(){
		this->timer.expires_from_now(boost::posix_time::milliseconds(this->heartbeatTimeout));
		this->timer.async_wait(boost::bind(&TimerHandler::handleExpiredStream, this->timerHandler, boost::asio::placeholders::error, this->conID, this->lsID));
	}

    void IncomingStreamImpl::expire(){
    	this->timer.cancel();
    }
}
