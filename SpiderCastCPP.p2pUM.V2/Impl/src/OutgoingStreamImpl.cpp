/*
 * OutgoingStreamImpl.cpp
 *
 *  Created on: Mar 5, 2013
 *      Author: sagibel
 */

#include "OutgoingStreamImpl.h"

#include <boost/bind.hpp>

namespace p2pum
{
	OutgoingStreamImpl::OutgoingStreamImpl(/*Type type, */ConnectionID conID, const PropertyMap& props, TimerHandler_SPtr handler, IOService_SPtr service): AbstractStream(conID, props), timer(*service){
		PropertyMap::const_iterator it = props.find("heartbeat_interval");
		if(it == props.end()){
			this->heartbeatInterval = STREAM_HEARTBEAT_INTERVAL;
		}
		else{
			this->heartbeatInterval = atoi(it->second.c_str());
		}
		this->timerHandler = handler;
		refresh();
	}

	OutgoingStreamImpl::~OutgoingStreamImpl(){}

	long OutgoingStreamImpl::timeToRefresh() const{
		return this->timer.expires_from_now().total_milliseconds();
	}

	bool OutgoingStreamImpl::isFresh() const{
		return (timeToRefresh() > 0);
	}

	void OutgoingStreamImpl::refresh(){
		this->timer.expires_from_now(boost::posix_time::milliseconds(this->heartbeatInterval));
		this->timer.async_wait(boost::bind(&TimerHandler::handleStreamRefresh, this->timerHandler, boost::asio::placeholders::error, this->conID, this->lsID));
	}

    void OutgoingStreamImpl::stale(){
    	this->timer.cancel();
    }
}
