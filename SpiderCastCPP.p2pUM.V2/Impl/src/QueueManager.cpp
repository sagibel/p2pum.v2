/*
 * QueueManager.cpp
 *
 *  Created on: Feb 6, 2013
 *      Author: sagibel
 */

#include "QueueManager.h"

#include <boost/lexical_cast.hpp>

namespace p2pum{

	QueueManager::QueueManager(const PropertyMap& properties){
		if(properties.count("max_events")){
			this->eventQueueSize = boost::lexical_cast<unsigned int>(properties.find("max_events")->second);
		}
		else{
			this->eventQueueSize = EVENT_QUEUE_SIZE;
		}
		if(properties.count("max_messages")){
			this->messageQueueSize = boost::lexical_cast<unsigned int>(properties.find("max_messages")->second);
		}
		else{
			this->messageQueueSize = MESSAGE_QUEUE_SIZE;
		}
		if(properties.count("max_logs")){
			this->logQueueSize = boost::lexical_cast<unsigned int>(properties.find("max_logs")->second);
		}
		else{
			this->logQueueSize = LOG_QUEUE_SIZE;
		}
	}

	QueueManager::~QueueManager(){}

	void QueueManager::pushEvent(Event_SPtr& event){
		boost::unique_lock<boost::mutex> lock(eventMutex);
		while((int)eventQueue.size() >= this->eventQueueSize){
			eventQueueCond.wait(lock);
		}
		eventQueue.push(event);
		lock.unlock();
		anyQueueCond.notify_one(); //notify communicator
	}

	Event_SPtr QueueManager::popEvent(){
		boost::unique_lock<boost::mutex> lock(eventMutex);
		while(eventQueue.empty()){
			eventQueueCond.wait(lock);
		}
		Event_SPtr ret = eventQueue.front();
		eventQueue.pop();
		lock.unlock();
		eventQueueCond.notify_one(); //notifies either Handler or Maintainer
		return ret;
	}

	void QueueManager::pushMessage(RxMessage_SPtr& message){
		boost::unique_lock<boost::mutex> lock(messageMutex);
		while((int)messageQueue.size() >= this->messageQueueSize){
			messageQueueCond.wait(lock);
		}
		messageQueue.push(message);
		lock.unlock();
		anyQueueCond.notify_one(); //notifies Communicator
	}

	RxMessage_SPtr QueueManager::popMessage(){
		boost::unique_lock<boost::mutex> lock(messageMutex);
		while(messageQueue.empty()){
			messageQueueCond.wait(lock);
		}
		RxMessage_SPtr ret = messageQueue.front();
		messageQueue.pop();
		lock.unlock();
		messageQueueCond.notify_one(); //notifies either Listener or Maintainer
		return ret;
	}

	void QueueManager::pushLog(Log_SPtr& log){
		boost::unique_lock<boost::mutex> lock(logMutex);
		while((int)logQueue.size() >= this->logQueueSize){
			logQueueCond.wait(lock);
		}
		logQueue.push(log);
		lock.unlock();
		anyQueueCond.notify_one(); //notifies Communicator
	}

	Log_SPtr QueueManager::popLog(){
		boost::unique_lock<boost::mutex> lock(logMutex);
		while(logQueue.empty()){
			logQueueCond.wait(lock);
		}
		Log_SPtr ret = logQueue.front();
		logQueue.pop();
		lock.unlock();
		logQueueCond.notify_one(); //notifies either Listener or Maintainer
		return ret;
	}

	bool QueueManager::hasEvents(){
		boost::unique_lock<boost::mutex> lock(eventMutex);
		bool ret = !eventQueue.empty();
		lock.unlock();
		return ret;
	}

	bool QueueManager::hasMessages(){
		boost::unique_lock<boost::mutex> lock(messageMutex);
		bool ret = !messageQueue.empty();
		lock.unlock();
		return ret;
	}

	bool QueueManager::hasLogs(){
		boost::unique_lock<boost::mutex> lock(logMutex);
		bool ret = !logQueue.empty();
		lock.unlock();
		return ret;
	}


	void QueueManager::waitForAny(){
		boost::unique_lock<boost::mutex> lock(anyMutex);
		anyQueueCond.wait(lock);
		lock.unlock();
	}
}
