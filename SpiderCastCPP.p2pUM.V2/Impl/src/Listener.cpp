/*
 * Listener.cpp
 *
 *  Created on: Jan 4, 2013
 *      Author: sagibel
 */

#include "Listener.h"
#include "ConnectionImpl.h"
#include "Connectable.h"

#include <boost/bind.hpp>

namespace p2pum
{

Listener::Listener(IOService_SPtr _service, int _port, TimerHandler_SPtr _handler): serverSocket(*_service, tcp::endpoint(tcp::v4(), _port)){
	this->service = _service;
	this->handler = _handler;
}


Listener::~Listener()
{
}

void Listener::listen(Handler_SPtr handler)
{
	ConnectionImpl* conImpl = new ConnectionImpl(this->service, handler, this->handler);
	Connectable_SPtr con(conImpl);
	conImpl->addLocalServerPort(this->serverSocket.local_endpoint().port());
	serverSocket.async_accept(con->getSocket(), boost::bind(&ConnectionHandler::handleAccept, handler, boost::dynamic_pointer_cast<Connection, Connectable>(con), boost::asio::placeholders::error));
}

TimerHandler_SPtr Listener::getTimerHandler(){
	return this->handler;
}

IOService_SPtr Listener::getIOService(){
	return this->service;
}

void Listener::close(){
	serverSocket.cancel();
	serverSocket.close();
}

}
