/**********************************************************************
 * Licensed Materials - Property of IBM
 *
 * (C) Copyright IBM Corp. 2012.  All Rights Reserved.
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 **********************************************************************
 */

/*
 * Event.h
 *
 *  Created on: Mar 25, 2012
 *      Author: tock
 */

#ifndef EVENT_H_
#define EVENT_H_

#include "P2PUM_Definitions.h"

namespace p2pum
{

enum EventReturnCode
{
	Event_RC_Reject = -1,
	Event_RC_NoOp 	= 0,
	Event_RC_Accept = 1
};

class Event
{
public:
	Event();
	virtual ~Event();

	enum Type
	{
		/**
		 *
		 */
		Fatal_Error, //0

		//=== connection events, initiator ===

		/**
		 * Outgoing establish connection succeeded, connection is ready to use
		 *
		 * Use getConnection() to access the connection.
		 */
		Connection_Establish_Success, //1

		/**
		 * Outgoing establish connection failed
		 *
		 * Use getConnection() to access the connection parameters.
		 */
		Connection_Establish_Failure, //2

		/**
		 * Outgoing establish connection failed due to establish-timeout
		 *
		 * Use getConnection() to access the connection parameters.
		 */
		Connection_Establish_Timeout, //3

		//=== connection events, acceptor ===

		/**
		 * New incoming connection, accept or reject
		 *
		 * Use getConnection() to access the connection parameters and connect message,
		 * return Event_RC_Reject for reject, Event_RC_Accept for accept.
		 */
		Connection_New, //4

		/**
		 * New incoming connection, ready to use
		 *
		 * Use getConnection() to access the connection.
		 */
		Connection_Ready, //5

		//=== connection events, life-cycle ===

		/**
		 * Connection unexpectedly broke
		 *
		 * Use getConnection() to access the connection parameters.
		 */
		Connection_Broke, //6

		/**
		 * Connection gracefully closed
		 *
		 * Use getConnection() to access the connection parameters.
		 */
		Connection_Closed, //7

		/**
		 * Heart-beat time-out expired
		 *
		 * Use getConnection() to access the connection parameters.
		 */
		Connection_Heartbeat_Timeout, //8

		//=== StreamRx events ===

		/**
		 * New incoming stream, accept or reject.
		 *
		 * Use getStreamID() to get the relevant StreamID.
		 * Use getConnection() to get the relevant Connection.
		 * Use getStreamTag() to get the stream tag.
		 *
		 * Return Event_RC_Reject for reject, Event_RC_Accept for accept.
		 */
		Rx_New_Source, //9

		/**
		 * Use getStreamID() to get the relevant StreamID.
		 * Use getConnection() to get the relevant Connection.
		 * Use getStreamTag() to get the stream tag.
		 */
		Rx_Transmitter_Closed, //10

		/**
		 * Use getStreamID() to get the relevant StreamID.
		 * Use getConnection() to get the relevant Connection.
		 * Use getStreamTag() to get the stream tag.
		 */
		Rx_Heartbeat_Timeout, //11

		//=== StreamTx events ===

		/**
		 * Use getStreamID() to get the relevant StreamID.
		 * Use getConnectionID() to get the relevant ConnectionID.
		 * Use getStreamTag() to get the stream tag.
		 */
		Tx_Receiver_Closed_Stream, //12

		//REMARK added
		Tx_Receiver_Rejected, //13

		//... etc, TBD

		//==== ADDED ======

		/**
		 * error event regarding incoming traffic over connection. for specification use getErrorCode and getErrorMessage.
		 */
		Connection_Read_Error, //14

		/**
		 * error event regarding outgoing traffic over connection. for specification use getErrorCode and getErrorMessage.
		 */
		Connection_Write_Error, //15

		/**
		 * error event regarding a connections that's not related to I/O. for specification use getErrorCode and getErrorMessage.
		 */
		Connection_Error, //16

		/**
		 * error event regarding stream. for specification use getErrorCode and getErrorMessage.
		 */
		Stream_Error //17
	};

	virtual Type getType() const = 0;

	virtual StreamID getStreamID() const = 0;

	virtual ConnectionID getConnectionID() const = 0; //REMARK redundant if there's Connection_SPtr...

	virtual Connection_SPtr getConnection() const = 0;

	virtual std::string getStreamTag() const = 0; //REMARK what is a stream tag?

	virtual ErrorCode getErrorCode() const = 0;

	virtual std::string getErrorMessge() const = 0;

	//... etc, TBD

};

}

#endif /* EVENT_H_ */
