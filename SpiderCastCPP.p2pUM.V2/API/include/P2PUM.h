/**********************************************************************
 * Licensed Materials - Property of IBM
 *
 * (C) Copyright IBM Corp. 2012.  All Rights Reserved.
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 **********************************************************************
 */

/*
 * P2PUM.h
 *
 *  Created on: Mar 25, 2012
 *      Author: tock
 */

#ifndef P2PUM_H_
#define P2PUM_H_

#include "P2PUM_Definitions.h"
#include "EventListener.h"
#include "ConnectionParams.h"
#include "MessageListener.h"
#include "P2PUMException.h"
#include "LogListener.h"

namespace p2pum {


class P2PUM {
public:
	virtual ~P2PUM();

	/**
	 * Create a P2PUM instance.
	 *
	 * Multiple instances can be created in the same process.
	 *
	 * Thread safe.
	 *
	 * @param properties must: "port" = port number for incoming connections. may: "max_cons" = max connections, "max_streams" = max streams per connection, TBD...
	 * @param eventListener
	 * @param logListener
	 * @return
	 */
	static P2PUM_SPtr createInstance(
			const PropertyMap & properties,
			EventListener* eventListener,
			LogListener* logListener) throw(P2PUMException);

	virtual void close() = 0;

	/**
	 * Asynch connect, returns result in an Event via the EventListener.
	 */
	virtual void establishConnection(const ConnectionParams & params)
	throw(P2PUMException) = 0;

	/**
	 * Use "heartbeat_timeout" and "heartbeat_interval" to specify
	 */
	virtual StreamTx_SPtr createStreamTx(
			const PropertyMap & properties,
			Connection_SPtr connection)
	throw(P2PUMException) = 0;

	virtual StreamRx_SPtr createStreamRx( //REMARK what the property map is needed for here?
			const PropertyMap & properties,
			MessageListener* messageListener)
	throw(P2PUMException) = 0;

protected:
	P2PUM();

};

}

#endif /* P2PUM_H_ */
