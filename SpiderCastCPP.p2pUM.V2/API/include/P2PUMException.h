/**********************************************************************
 * Licensed Materials - Property of IBM
 *
 * (C) Copyright IBM Corp. 2012.  All Rights Reserved.
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 **********************************************************************
 */

/*
 * P2PUMException.h
 *
 *  Created on: Mar 27, 2012
 *      Author: tock
 */

#ifndef P2PUMEXCEPTION_H_
#define P2PUMEXCEPTION_H_

#include <exception>
#include "P2PUM_Definitions.h"

namespace p2pum
{

class P2PUMException : public std::exception
{
public:
	P2PUMException(const std::string what, ErrorCode errorCode) throw();

	virtual ~P2PUMException() throw();

	virtual const char* what() const throw();

	virtual ErrorCode getErrorCode() const throw();

protected:
	std::string what_;
	ErrorCode errorCode_;

};

}

#endif /* P2PUMEXCEPTION_H_ */
