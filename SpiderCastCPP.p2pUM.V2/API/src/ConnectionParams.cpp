/**********************************************************************
 * Licensed Materials - Property of IBM
 *
 * (C) Copyright IBM Corp. 2012.  All Rights Reserved.
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 **********************************************************************
 */

/*
 * ConnectionParams.cpp
 *
 *  Created on: Mar 25, 2012
 *      Author: tock
 */

#include "ConnectionParams.h"

namespace p2pum
{

ConnectionParams::ConnectionParams()
{}

ConnectionParams::~ConnectionParams()
{}

}
