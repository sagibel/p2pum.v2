/**********************************************************************
 * Licensed Materials - Property of IBM
 *
 * (C) Copyright IBM Corp. 2012.  All Rights Reserved.
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 **********************************************************************
 */

/*
 * LogListener.cpp
 *
 *  Created on: Nov 8, 2012
 *      Author: tock
 */

#include "LogListener.h"

namespace p2pum
{

LogListener::LogListener()
{}

LogListener::~LogListener()
{}

}
